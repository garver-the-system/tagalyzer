/*
 * Copyright 2023 Alexander Garver
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use tagalyzer::prelude::*;

fn setup_args(args: Option<&[&str]>) -> Args {
	let mut mock_stdin = vec!["tagalyzer"];

	match args {
		Some(args) => {
			for arg in args {
				mock_stdin.push(arg);
			}
		}
		None => {
			mock_stdin.push("LICENSE-APACHE");
			mock_stdin.push("LICENSE-MIT");
		}
	}

	Args::parse_from(mock_stdin)
}

#[test]
fn file_processor_from_args() {
	let args = setup_args(None);
	let fp = FileProcessor::try_from(&args).unwrap();

	let mut stats = FileStatistics::new(&args.paths[0]);
	fp.populate_file_statistics(&mut stats).unwrap();

	assert_eq!(stats.get_counts()["license"], 26);
	assert_eq!(stats.get_counts()["derivative"], 19);
	assert_eq!(stats.get_counts().len(), 400);
}

#[test]
fn tagalyzer_from_args() {
	let args = setup_args(None);
	let tag = Tagalyzer::try_from(&args).unwrap();

	assert_eq!(tag.get_file_queue().len(), 2);
	assert_eq!(tag.get_file_statistics().len(), 0);

	let file_queue: Vec<&str> = tag
		.get_file_queue()
		.iter()
		.map(|stat| stat.name.as_str())
		.collect();
	assert!(
		file_queue.contains(&"LICENSE-MIT"),
		"File Queue: {file_queue:?}"
	);
	assert!(
		file_queue.contains(&"LICENSE-APACHE"),
		"File Queue: {file_queue:?}"
	);
}
