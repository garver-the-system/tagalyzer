/*
 * Copyright 2023 Alexander Garver
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! # Tagalyzer
//!
//! Tagalyzer is a text-analysis library intended to aid writers in finding appropriate tags for
//! their content. As of now, it counts words in a list of files, performing certain
//! character-based filtering in an attempt to remove formatting characters and punctuation.
//!
//! ## Examples
//!
//! To see a basic example without installing the CLI tool, try running one of these:
//!
//! ```bash
//! cargo run -- README.md
//! ```
//!
//! ```bash
//! cargo run -- --help
//! ```
//!
//! ## Library Usage
//!
//! There are two main ways to use this library. The simpler one is to use the `FileProcessor`
//! struct:
//!
//! ```
//! use tagalyzer::prelude::*;
//!
//! let fp = FileProcessor::default();
//! let word_counts = fp.count_words_in_file("README.md").unwrap();
//!
//! assert!(word_counts.get("words").unwrap() > &0)
//! ```
//!
//! Alternatively, you can instantiate a `Tagalyzer` from an instance of `Args` by supplying a faux
//! CLI input with as many files as you want.
//!
//! ```
//! use tagalyzer::prelude::*;
//!
//! let mut tag = Tagalyzer::new(&["LICENSE-APACHE", "LICENSE-MIT"], FileProcessor::default());
//!
//! let _ = tag.count_all_files().unwrap();
//! let tag = tag.sort();
//! print!("{tag}");
//! ```

mod args;
mod file_processor;
mod statistics;

use std::{error::Error, fmt::Display, fs, io::Error as IoError, path::PathBuf};

use glob::glob;

pub use args::prelude::*;
pub use file_processor::prelude::*;
pub use statistics::prelude::*;

pub mod prelude {
	pub use super::*;
}

/// # Tagalyzer
///
/// This is the base class of the library. It orchestrates the consistent analysis across and
/// between different text files.
///
/// At the moment, it simply populates a list of files to perform analysis on and creates a
/// template processor, then uses that processor to iterate over the list of files in serial.
///
/// In the future, there will be an option to handle this analysis concurrently, likely utilizing
/// a thread pool to avoid necessitating an async/await runtime.
#[derive(Debug, Clone)]
pub struct Tagalyzer<State = Unsorted> {
	/// # File Queue
	///
	/// The queue of files that need to be processed, stored as `FileStatistics<Unsorted>` to
	/// utilize a consistent data structure and compile-time type safety.
	file_queue: Vec<FileStatistics<Unsorted>>,
	/// # Sorted Files
	///
	/// Files that have been sorted. Again, `FileStatistics<Sorted>` ensures type safety and a
	/// consistent data structure.
	counted_files: Vec<FileStatistics<State>>,
	/// # File Processor
	///
	/// An instance of the `FileProcessor` struct that is used as a "source of truth" for how
	/// analysis is to be performed. This ensures consistent and repeatable analysis.
	file_processor: FileProcessor,
}

impl<State> Tagalyzer<State> {
	/// # Get File Statistics
	///
	/// A simple getter for the counted files.
	#[must_use]
	pub const fn get_file_statistics(&self) -> &Vec<FileStatistics<State>> {
		&self.counted_files
	}

	/// # Get File Queue
	///
	/// A simple getter for the uncounted files.
	#[must_use]
	pub const fn get_file_queue(&self) -> &Vec<FileStatistics<Unsorted>> {
		&self.file_queue
	}
}

impl Tagalyzer<Unsorted> {
	/// # New
	///
	/// Instantiates a new Tagalyzer with a file queue based on the passed arg.
	#[must_use]
	pub fn new(paths: &[&str], file_processor: FileProcessor) -> Self {
		let file_queue = paths.iter().map(|path| FileStatistics::new(path)).collect();

		Self {
			file_queue,
			counted_files: Vec::default(),
			file_processor,
		}
	}
	/// # Count All Files
	///
	/// Iterates over the file queue and uses the file processor to perform the same analysis on
	/// each one, then sorts the statistics and adds it to the sorted files list.
	///
	/// In the future, this is where the core of the concurrency logic will be.
	///
	/// ## Errors
	///
	/// Transparently returns any `std::io::Error` which may arise. Such an error indicates a
	/// problem opening a file in read mode.
	///
	/// This may indicate a directory path was passed as a file, which isn't currently supported.
	/// Use a subshell (or similar) to list or walk the directory contents.
	pub fn count_all_files(&mut self) -> Result<(), IoError> {
		while let Some(mut stats) = self.file_queue.pop() {
			self.file_processor.populate_file_statistics(&mut stats)?;
			self.counted_files.push(stats);
		}
		Ok(())
	}

	/// # Sort
	///
	/// Sorts the `FileStatistics` vector, finalizing the list of words in each file.
	#[must_use]
	pub fn sort(self) -> Tagalyzer<Sorted> {
		let mut sorted_files = Vec::new();
		for stats in self.counted_files {
			let stats = stats.sort();
			sorted_files.push(stats);
		}

		Tagalyzer::<Sorted> {
			file_queue: Vec::new(),
			counted_files: sorted_files,
			file_processor: self.file_processor,
		}
	}

	/// # Add File to File Queue
	///
	/// Given the path to a file, this function instantiates a `FileStatistics` struct and pushes it
	/// to the queue of files to sort.
	pub fn add_file_to_queue(&mut self, path: &str) {
		let new_file = FileStatistics::new(path);
		self.file_queue.push(new_file);
	}

	/// # Recurse Directories
	///
	/// Recursively search a directory and return a list of all found files.
	fn recurse_dir(path: &PathBuf) -> Result<Vec<PathBuf>, IoError> {
		let mut paths = Vec::new();
		for path in fs::read_dir(path)? {
			let entry = path?;
			if entry.file_type()?.is_dir() {
				for entry in fs::read_dir(entry.path())? {
					let entry = entry?;
					if entry.file_type()?.is_dir() {
						let mut recursed_paths = Self::recurse_dir(&entry.path())?;
						paths.append(&mut recursed_paths);
					} else {
						paths.push(entry.path());
					}
				}
			} else {
				paths.push(entry.path());
			}
		}

		return Ok(paths);
	}

	/// # Expand Paths
	///
	/// This function expands paths from strings to `PathBuf`s. This includes glob expansion and
	/// directory recursion.
	///
	/// ## Errors
	///
	/// Both globs and recursion can generate errors, either `GlobError` or `std::io::Error`
	/// respectively. These are passed up transparently.
	fn expand_paths(str_paths: &[String], recurse: bool) -> Result<Vec<PathBuf>, Box<dyn Error>> {
		let mut collected_paths = Vec::new();

		for path in str_paths {
			if path.contains('*') {
				let mut paths: Vec<PathBuf> =
					glob(path)?.filter_map(std::result::Result::ok).collect();
				collected_paths.append(&mut paths);
			}

			let buf = PathBuf::from(path);
			if buf.is_file() {
				collected_paths.push(buf);
			} else if recurse && buf.is_dir() {
				let mut recursed_paths = Self::recurse_dir(&buf)?;
				collected_paths.append(&mut recursed_paths);
			}
		}

		return Ok(collected_paths);
	}
}

impl Default for Tagalyzer {
	fn default() -> Self {
		Self {
			file_queue: Vec::default(),
			counted_files: Vec::default(),
			file_processor: FileProcessor::default(),
		}
	}
}

impl TryFrom<&Args> for Tagalyzer {
	type Error = Box<dyn Error>;

	fn try_from(args: &Args) -> Result<Self, Self::Error> {
		let file_queue = Self::expand_paths(&args.paths, args.recurse)?
			.iter()
			.map(|path| FileStatistics::new(&path.to_string_lossy()))
			.collect();

		let ret = Self {
			file_queue,
			counted_files: Vec::new(),
			file_processor: FileProcessor::try_from(args)?,
		};
		Ok(ret)
	}
}

impl Display for Tagalyzer<Sorted> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		for file in &self.counted_files {
			writeln!(f, "{file}")?;
		}
		Ok(())
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::{
		env, fs,
		io::ErrorKind::{AlreadyExists as FileExists, NotFound},
		path::PathBuf,
	};

	use rand::random;

	#[test]
	fn test_new() {
		let tagalyzer = Tagalyzer::new(&["Cargo.toml"], FileProcessor::default());

		// Assert Tagalyzer initialized correctly
		assert_eq!(tagalyzer.file_queue.len(), 1);
		assert_eq!(tagalyzer.counted_files.len(), 0);
	}

	#[test]
	fn test_count_all_files() {
		let file1 = setup_file();
		let file2 = setup_file();

		let files = [file1.to_str().unwrap(), file2.to_str().unwrap()];
		let mut tagalyzer = Tagalyzer::new(&files, FileProcessor::default());

		assert_eq!(tagalyzer.file_queue.len(), 2);
		assert_eq!(tagalyzer.counted_files.len(), 0);

		let result = tagalyzer.count_all_files();
		assert!(result.is_ok());

		assert_eq!(tagalyzer.file_queue.len(), 0);
		assert_eq!(tagalyzer.counted_files.len(), 2);

		let stats1 = tagalyzer.counted_files.pop().unwrap();
		let stats2 = tagalyzer.counted_files.pop().unwrap();
		assert_eq!(stats1, stats2);
	}

	#[test]
	fn file_does_not_exist() {
		let mut tagalyzer = Tagalyzer::new(&["foobar"], FileProcessor::default());

		let result = tagalyzer.count_all_files();

		assert!(result.is_err());

		let e = result.err().unwrap();
		assert_eq!(e.kind(), NotFound)
	}

	#[test]
	fn new_with_empty_files() {
		let tagalyzer = Tagalyzer::new(&[], FileProcessor::default());

		assert_eq!(tagalyzer.file_queue.len(), 0);
	}

	#[test]
	fn count_nonexistent_file() {
		let mut tagalyzer = Tagalyzer::default();
		tagalyzer
			.file_queue
			.push(FileStatistics::new("nonexistent.txt"));

		let result = tagalyzer.count_all_files();
		assert!(result.is_err());
	}

	#[test]
	fn sort_with_no_files() {
		let tagalyzer = Tagalyzer::default();

		let sorted = tagalyzer.sort();
		assert_eq!(sorted.counted_files.len(), 0);
	}

	#[test]
	fn tagalyzer_recursive() {
		let files: Vec<String> = Tagalyzer::expand_paths(&["tests".to_owned()], true)
			.unwrap()
			.iter()
			.map(|path| path.to_string_lossy().to_string())
			.collect();

		assert!(
			files.contains(&"tests/assets/text.txt".to_string()),
			"File Queue: {files:?}"
		);
	}

	#[test]
	fn tagalyzer_glob() {
		let files: Vec<String> = Tagalyzer::expand_paths(&["LICENSE-*".to_owned()], false)
			.unwrap()
			.iter()
			.map(|path| path.to_string_lossy().to_string())
			.collect();

		assert!(
			files.contains(&"LICENSE-MIT".to_string()),
			"File Queue: {files:?}"
		);
		assert!(
			files.contains(&"LICENSE-APACHE".to_string()),
			"File Queue: {files:?}"
		);
	}

	#[test]
	fn tagalyzer_double_glob() {
		let files: Vec<String> = Tagalyzer::expand_paths(&["tests/**/*".to_owned()], false)
			.unwrap()
			.iter()
			.map(|path| path.to_string_lossy().to_string())
			.collect();

		assert!(
			files.contains(&"tests/assets/text.txt".to_string()),
			"File Queue: {files:?}"
		);
	}

	#[test]
	fn tagalyzer_glob_filter() {
		let files: Vec<String> = Tagalyzer::expand_paths(&["tests/**/*.txt".to_owned()], false)
			.unwrap()
			.iter()
			.map(|path| path.to_string_lossy().to_string())
			.collect();

		assert!(
			files.contains(&"tests/assets/text.txt".to_string()),
			"Files: {files:?}"
		);
		assert!(
			!files.contains(&"tests/assets/text.md".to_string()),
			"Files: {files:?}"
		);
	}

	#[test]
	fn directory_no_recurse() {
		let files = Tagalyzer::expand_paths(&["tests/assets".to_owned()], false).unwrap();

		assert_eq!(files.len(), 0);
	}

	/// # Generate Filepath
	///
	/// Utility function to generate a random filepath within a given directory.
	fn gen_filepath(dir: &PathBuf) -> PathBuf {
		let mut filepath = dir.clone();
		filepath.push(format!("text_{}.txt", random::<usize>()));
		return filepath;
	}

	/// # Setup File
	///
	/// Utility function to set up tests with a text file.
	fn setup_file() -> PathBuf {
		let contents: &str = "But Mousie, thou art no thy-lane,\n\
									In proving foresight may be vain:\n\
									The best laid schemes o' Mice an' Men\n\
									Gang aft agley,\n\
									An' lea'e us nought but grief an' pain,\n\
									For promis'd joy!";

		let mut pwd = env::temp_dir();
		pwd.push("tagalyzer_tests");
		if let Err(e) = fs::DirBuilder::new().create(&pwd) {
			if e.kind() != FileExists {
				panic!()
			}
		};

		let filepath = gen_filepath(&pwd);

		fs::write(&filepath, &contents).unwrap();
		filepath
	}
}
