/*
 * Copyright 2023 Alexander Garver
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! # Tagalyzer Binary
//!
//! This is the binary package for the Tagalyzer CLI.

use tagalyzer::prelude::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
	let args = Args::parse();

	let mut tagalyzer = Tagalyzer::try_from(&args)?;
	tagalyzer.count_all_files()?;
	let tagalyzer = tagalyzer.sort();

	println!("{tagalyzer}");

	Ok(())
}
