/*
 * Copyright 2023 Alexander Garver
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//! # Args
//!
//! Command line arguments for the word counter. Most of the functionality is provided by `clap`,
//! this is just our implementation.

use clap::{ArgAction, Parser};

// Allowed for prelude specificlally
#[allow(clippy::missing_docs_in_private_items)]
pub mod prelude {
	pub use super::Args;
	pub use clap::Parser;
}

#[derive(Clone, Debug, Parser)]
pub struct Args {
	/// Count words as case-sensitive. I.e. "Mousie" != "mousie".
	#[clap(short, long, action)]
	pub count_case: bool,

	/// Filter words as case-sensitive. Turns off the Regex `i` flag.
	#[clap(short = 'i', long, action=ArgAction::SetFalse)]
	pub filter_case: bool,

	#[clap(short, long, action)]
	/// Recurse directories passed in paths.
	pub recurse: bool,

	/// Regex to filter out
	#[clap(
		short,
		long,
		default_value = r##"[^\s|\w]|\b(?:a|an|the|if|of|and|or|but|to|in|am|are|is|be|do|as)\b"##
	)]
	pub filter_regex: String,

	/// Regex to replace with whitespace
	#[clap(short, long, default_value = r##"[^\s|\w]+"##)]
	pub delimit_regex: String,

	/// File or directory paths to count words from
	pub paths: Vec<String>,
}
